# MediDJ-Website-2016
Exam website by Teodor Moquist

## Notes
 - [Trello](https://trello.com/b/thoUdxKW/medidj)
 - [Website](http://medidj-94162.onmodulus.net/)

## Requirements
 - [RubyInstaller](http://rubyinstaller.org/)
 - [Sass](http://sass-lang.com/)
 - [Compass](http://compass-style.org/)
 - [NodeJS](https://nodejs.org/en/)
 - [Bower.io](https://bower.io/)
 - [Grunt](http://gruntjs.com/)
 - [MongoDB](https://www.mongodb.com/)

## Installation
 - Clone Project
 - Open Command Prop and `cd` to project
 - Run `npm install` and `bower install` for installing packages

## Run Project
- Open Command Prop
- Type `mongod`
- Open another Command Prop and `cd` to project
- Type `grunt serve` for preview