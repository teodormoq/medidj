'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ThingSchema = new Schema({
  name: String,
  rating: String,
  album: Object,
  artists: Array,
  uri: String,
  url: String,
  external_urls: Object
});

module.exports = mongoose.model('Thing', ThingSchema);
