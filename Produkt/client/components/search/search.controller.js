(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('SearchCtrl', SearchCtrl);

  /* @ngInject */
  function SearchCtrl(Spotify, $scope, $rootScope) {
    $scope.searchBox = false;

    // Search Function
    $scope.btnSearch = function() {
      Spotify.search($scope.spotifySearch, 'track').then(function (data) {
        $scope.searchBox = true;
        $scope.searchPlaylist = data;

        // Addsong to main playlist button
        $scope.addSearchSong = function($index) {
          var trackSelected = data.tracks.items[$index].uri;

          Spotify.addPlaylistTracks('1125218256', '7s8iKXcStmEEpB2KQpOfuk', trackSelected).then(function () {
            alert('Track added');
          });
        };
      });
    };

    // Close search overlay
    $scope.btnSearchClose = function() {
      $scope.searchBox = false;
    }

    // Open Menu to a big element
    $rootScope.isBurgerMenu = false;
    $scope.btnBurgerMenu = function() {
      $rootScope.isBurgerMenu = !$rootScope.isBurgerMenu;
      console.log('dsadsa');
    };
  }
})();
