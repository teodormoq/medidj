(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('CookiesCtrl', CookiesCtrl);

  /* @ngInject */
  function CookiesCtrl($scope) {
    $scope.cookiespopupshow = true;
    $scope.cookiespopup = function() {
      $scope.cookiespopup = localStorage.setItem('cookiespopup', 'accepted');
      $scope.cookiespopupshow = false;
    }
    if("cookiespopup" in localStorage) {
      $scope.cookiespopupshow = false;
    }
  }
})()
