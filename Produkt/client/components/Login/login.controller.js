(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('LoginCtrl', LoginCtrl);

  /* @ngInject */
  function LoginCtrl($scope, $interval, Spotify, $rootScope) {
    // Login function
    $scope.loginSpotify = function () {
      Spotify.login().then(function () {
        window.location.reload();
      }, function () {
        console.log("didn\'t log in");
      });
    };

    // Variable to deactivate class 'deactivate' on btn-spotify
    $rootScope.isSpotifyLoggedIn = false;

    // Check if spotify token is in localstorage
    if ("spotify-token" in localStorage) {
      $rootScope.spotifyLoggedIn = true;
      $rootScope.isSpotifyLoggedIn = !$rootScope.isSpotifyLoggedIn;

      // Deleting the spotify token from localstorge after 1 hour
      $interval( function(){
        delete localStorage['spotify-token'];
        alert("You're access to spotify is timeouted, try login again.");
        window.location.reload();
      }, 3600000);

      // Check every 5 sec if spotify token is expired
      $interval( function(){
        if ($scope.currentUser == undefined) {
          delete localStorage['spotify-token'];
          window.location.reload();
        } else {}
      }, 5000);
    }
    else {
      $rootScope.spotifyLoggedIn = false;
    }
  }

})();
