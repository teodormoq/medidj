(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('NavbarCtrl', NavbarCtrl);

  /* @ngInject */
  function NavbarCtrl($scope, Spotify) {
    Spotify.getCurrentUser().then(function (data) {
      $scope.currentUser = data;
    });
  }

})();
