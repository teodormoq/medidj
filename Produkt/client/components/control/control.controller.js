(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('controlCtrl', controlCtrl);

  /* @ngInject */
  function controlCtrl(Spotify, $scope, ngAudio) {

    // Get infomation about loggedin user
    Spotify.getCurrentUser().then(function (data) {
      $scope.currentUser = data;
    });
  }
})();
