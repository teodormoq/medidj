'use strict';

describe('Filter: urlTrusted', function () {

  // load the filter's module
  beforeEach(module('produktApp'));

  // initialize a new instance of the filter before each test
  var urlTrusted;
  beforeEach(inject(function ($filter) {
    urlTrusted = $filter('urlTrusted');
  }));

  it('should return the input prefixed with "urlTrusted filter:"', function () {
    var text = 'angularjs';
    expect(urlTrusted(text)).toBe('urlTrusted filter: ' + text);
  });

});
