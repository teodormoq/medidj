(function() {

  'use strict';

  angular
    .module('produktApp')
    .filter('urlTrusted', UrlTrusted);

  /* @ngInject */
  function UrlTrusted($sce) {
    return function (url) {
      return $sce.trustAsResourceUrl(url);
    };
  }

})();
