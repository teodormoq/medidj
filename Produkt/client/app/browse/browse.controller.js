(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('BrowseCtrl', BrowseCtrl);

  function BrowseCtrl($scope, Spotify) {
    // Get infomation about loggedin user
    Spotify.getCurrentUser().then(function (data) {
      $scope.currentUser = data;

      // Get newest tracks
      Spotify.getNewReleases({ country: data.country }).then(function (data) {
        $scope.browse = data;
      });
    });

    // Get Medidj playlist tracks
    Spotify.getPlaylistTracks('1125218256', '7s8iKXcStmEEpB2KQpOfuk').then(function (data) {
      $scope.top5 = data;
    });
  }
})();
