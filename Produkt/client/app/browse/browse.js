(function () {

  'use strict';

  angular
    .module('produktApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('browse', {
        url: '/browse',
        templateUrl: 'app/browse/browse.html',
        controller: 'BrowseCtrl'
      });
  }
  
})();