(function() {

  'use strict';

  angular
    .module('produktApp')
    .service('MainSvc', MainSvc);

  /* @ngInject */
  function MainSvc($http) {
    this.getPlaylist = getPlaylist;
    this.addSong = addSong;
    this.updateSong = updateSong;
    this.deleteSong = deleteSong;

    function getPlaylist() {
      return $http.get('/api/things');
    }

    function addSong(song) {
      return $http.post('/api/things', song);
    }

    function updateSong(id, song) {
      return $http.put('/api/things/' + id, song);
    }

    function deleteSong(id) {
      return $http.delete('/api/things/' + id);
    }
  }

})();
