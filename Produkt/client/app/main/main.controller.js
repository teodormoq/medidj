(function () {

  'use strict';

  angular
  .module('produktApp')
  .controller('MainCtrl', MainCtrl);

  /* @ngInject */
  function MainCtrl($scope, Spotify) {

    // Get infomation about loggedin user
    Spotify.getCurrentUser().then(function (data) {
      $scope.currentUser = data;

      // Get newest playlists
      Spotify.getFeaturedPlaylists({ country: data.country }).then(function (data) {
        $scope.charts = data;
      });

      // Get newest tracks
      Spotify.getNewReleases({ country: data.country }).then(function (data) {
        $scope.browse = data;
      });
    });

    // Get newest recommendations
    Spotify.getRecommendations({ seed_artists: '4NHQUGzhtTLFvgF5SZesLK' }).then(function (data) {
      $scope.discover = data;

      // Addsong to main playlist button
      $scope.addSong = function($index) {
        var trackSelected = data.tracks[$index].uri;

        Spotify.addPlaylistTracks('1125218256', '7s8iKXcStmEEpB2KQpOfuk', trackSelected).then(function () {
          alert('Track added');
        });
      };
    });

    // Get Medidj playlist tracks
    Spotify.getPlaylistTracks('1125218256', '7s8iKXcStmEEpB2KQpOfuk').then(function (data) {
      $scope.top5 = data;
    });
}})
();
