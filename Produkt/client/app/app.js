(function() {

  'use strict';

  angular
    .module('produktApp', [
		  'ngCookies',
		  'ngResource',
		  'ngSanitize',
		  'ui.router',
		  'ui.bootstrap',
      'spotify',
      'angularUtils.directives.dirPagination',
      'ngAudio'
		])
    .config(config);

  /* @ngInject */
  function config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, SpotifyProvider) {

    $locationProvider.html5Mode(true);

    // Set access to spotify logon
    SpotifyProvider.setClientId('ae2438bc2f95463fab3b33f603ef0423');
    SpotifyProvider.setRedirectUri('http://localhost:9000/app/callback.html');
    SpotifyProvider.setScope('playlist-read-private playlist-modify-public user-library-read user-read-private user-read-email user-follow-modify playlist-read-collaborative playlist-modify-private user-library-modify user-read-birthdate user-follow-read user-top-read');
    // Get new token from callback.html, and set into a localstorage
    var token = localStorage.getItem('spotify-token');
    SpotifyProvider.setAuthToken(token);

    $urlRouterProvider
      .otherwise('/');
  }
})();
