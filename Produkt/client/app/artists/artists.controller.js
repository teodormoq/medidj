(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('ArtistsCtrl', ArtistsCtrl);

  function ArtistsCtrl($scope, Spotify) {
    // Get infomation about loggedin user
    Spotify.getCurrentUser().then(function (data) {
      $scope.currentUser = data;

      // Get Recommendation Artists
      // Spotify.getRecommendations({ seed_artists: '4NHQUGzhtTLFvgF5SZesLK' }).then(function (data) {
      //   $scope.artists = data;
      // });
      Spotify.getUserTopArtists({ limit: 10 }).then(function (data) {
        $scope.artists = data;
      });
    });

    // Get Medidj playlist tracks
    Spotify.getPlaylistTracks('1125218256', '7s8iKXcStmEEpB2KQpOfuk').then(function (data) {
      $scope.top5 = data;
    });
  }

})();
