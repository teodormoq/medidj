(function () {

  'use strict';

  angular
    .module('produktApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('artists', {
        url: '/artists',
        templateUrl: 'app/artists/artists.html',
        controller: 'ArtistsCtrl'
      });
  }
  
})();