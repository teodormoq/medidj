(function() {

  'use strict';

  angular
    .module('produktApp')
    .controller('PlaylistCtrl', PlaylistCtrl);

  function PlaylistCtrl($scope, Spotify, ngAudio, $http, MainSvc, $rootScope) {

    // Get infomation about loggedin user
    Spotify.getCurrentUser().then(function (data) {
      $scope.currentUser = data;
    });

    // Get Medidj playlist tracks
    Spotify.getPlaylistTracks('1125218256', '7s8iKXcStmEEpB2KQpOfuk').then(function (data) {

      // Add Songs to DB
      var tracks = data.items;

      init(tracks);
      function init(tracks) {
        // For each track
        angular.forEach(tracks, function(track) {

          // Get tracks from DB
          var foundDB = MainSvc.getPlaylist().success(function(dbTracks) {
            // Search DB Tracks if tracks from Spotfiy API is true
            var found = _.find(dbTracks, function (dbTrack) {return dbTrack.name === track.track.name});

            if (found) {
              $rootScope.top5 = dbTracks;
            } else {
              // if track not found then add track to DB
              MainSvc.addSong(
                // Add track + rating object
                angular.extend({}, track.track, {rating: 0})
              );
            }
          });
        });
      }

      // Get tracks from DB
      MainSvc.getPlaylist().success(function(dbTracks) {

        // Rating Songs up
        $scope.ratingClicked = false;
        $scope.uprating = function(song) {
          song.rating++;

          MainSvc.updateSong(song._id, {rating: song.rating})
          .error(function() {
            alert("We cound't access the db right now, please try again");
            song.rating--;
          })

          // Disabled button
          song.ratingClicked = true;
        };
      });

      // Get tracks from DB
      MainSvc.getPlaylist().success(function(dbTracks) {

        // Rating Songs up
        $scope.ratingClicked = false;
        // Rating Songs down
        $scope.downrating = function(song) {
          song.rating--;

          MainSvc.updateSong(song._id, {rating: song.rating})
          .error(function() {
            alert("We cound't access the db right now, please try again");
            song.rating++;
          })

          // Disabled button
          song.ratingClicked = true;
          // data.items[$index].track.ratingClicked = true;
        };
      });

      // Set Currentpage
      $scope.currentPage = 1;
      var pageSize = $scope.pageSize = 10;
      $scope.pageChangeHandler = function(num) {
        var currentPage = $scope.currentPage = num;

        // Play song on click
        $scope.playSong = function($index) {
          // Get number and + with currentpage - 1 * items per page
          var getCurrentNumber = ($index + 1) + (currentPage - 1) * pageSize;
          $scope.sound = ngAudio.load(data.items[getCurrentNumber].track.preview_url + '.mp3');
          $scope.isPlaying = !$scope.isPlaying;

          // Checking if song playing
          if ($scope.isPlaying == true) {
            // Plays song
            $scope.sound.play();
          } else {
            // Stops song
            $scope.sound.pause();
          }
        }
      };
    });
  }

})();
