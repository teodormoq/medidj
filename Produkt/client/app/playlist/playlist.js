(function () {

  'use strict';

  angular
    .module('produktApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('playlist', {
        url: '/playlist',
        templateUrl: 'app/playlist/playlist.html',
        controller: 'PlaylistCtrl'
      });
  }
  
})();